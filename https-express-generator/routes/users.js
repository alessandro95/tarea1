const express = require("express");
const router = express.Router();

const userController = require("../controllers/user");

router.get("/", (req, res) => {
  const listUser = userController.list();
  return res.json(listUser);
});

router.get("/:userId/show", (req, res) => {
  const { userId } = req.params;
  if (Number(userId)) {
    userController.show(Number(userId)).then(
      (user) => {
        res.json(user);
      },
      (error) => {
        res.status(500).json(error);
      }
    );
  } else {
    return res.status(500).json({ message: "UserId debe ser numerico" });
  }
});

router.post("/", (req, res) => {
  const { id, name, lastname } = req.body;
  const newUser = userController.create(id, name, lastname);
  return res.json(newUser);
});

router.delete("/:userId", (req, res) => {
  const { userId } = req.params;
  if (Number(userId)) {
    userController.delete(Number(userId)).then(
      (user) => {
        res.json(user);
      },
      (error) => {
        res.status(500).json(error);
      }
    );
  } else {
    return res.status(500).json({ message: "UserId debe ser numerico" });
  }
});

router.put("/:userId", (req,res) => {
  const { userId } = req.params;
  const{ name,lastname} = req.body;
  if (Number(userId)) {
  userController.update(Number(userId),name,lastname).then(
    (user) => {
      res.json(user);
    },
    (error) => {
      res.status(500).json(error);
    }
  );
  }else{
    return res.status(500).json({ message: "UserId debe ser numerico" });
  }
});


module.exports = router;
