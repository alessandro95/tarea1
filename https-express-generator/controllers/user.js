const listUser = [
  {
    id: 1,
    name: "Kelvin",
    lastname: "Carrión A.",
  },
  {
    id: 20,
    name: "Milagros",
    lastname: "Condori Q.",
  },
];

class User {
  list() {
    return listUser;
  }

  show(userId) {
    return new Promise((resolve, reject) => {
      const userEncontrado = listUser.find((user) => user.id === userId);
      if (userEncontrado) {
        return resolve(userEncontrado);
      } else {
        return reject({ message: "UserId no encontrado" });
      }
    });
  }

  create(id, name, lastname){
    const newUser = {
      id: id,
      name: name,
      lastname: lastname,
    }
    listUser.push(newUser);
    return newUser
  }

  delete(userId){
   return new Promise((resolve,reject) => {
      const userBorrar = listUser.findIndex((user) => user.id === userId);
      if (userBorrar>=0) {
        listUser.splice(userBorrar,1);
        return resolve(listUser);
      } else {
        return reject({ message: "UserId no encontrado" });
      }
   });
  }

  update(userId,name,lastname){
    return new Promise((resolve,reject) => {
      const userActualizar = listUser.find((user) => user.id === userId);
        if(userActualizar){
             userActualizar.lastname = lastname;
             userActualizar.name = name;
            return resolve(listUser); 
        }else{
          return reject({ message: "UserId no encontrado" });
        } 

    });
  }


}

module.exports = new User();
